﻿using PruebaTecnica.API.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PruebaTecnica.API.Data
{
    public class LibraryContext : DbContext
    {
        public LibraryContext() : base("name=DbContext")
        {
        }

        public DbSet<autores> autores { get; set; }
        public DbSet<autores_has_libros> autores_libros { get; set; }
        public DbSet<editoriales> editoriales { get; set; }
        public DbSet<libros> libros { get; set; }
    }
}
﻿using PruebaTecnica.API.Data;
using PruebaTecnica.API.Models;
using System.Linq;
using System.Web.Http;

namespace PruebaTecnica.API.Controllers
{
    public class BibliotecaController : ApiController
    {
        [HttpGet]
        public autores Get()
        {
            using (var context = new LibraryContext())
            {
                var pr = context.autores.ToList();
                return pr.FirstOrDefault();
            }
        }
    }
}
﻿using Microsoft.EntityFrameworkCore;
using PruebaTecnica.Dtos.Interfaces;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Infrastructure.Context;

namespace PruebaTecnica.Infrastructure.Methods
{
    public class AlmacenMethods : IAlmacen
    {
        private readonly AlmacenContext _context;

        public AlmacenMethods(AlmacenContext context)
        {
            _context = context;
        }

        public async Task<List<Producto>> GetProductos() => await _context.Producto.ToListAsync();
        public async Task<Producto> GetProductById(int idproducto) => await _context.Producto.Where(x => x.IdProducto == idproducto).FirstOrDefaultAsync();
        public async Task<List<Producto>> GetProductosByCategory(int idCategoria) => await _context.Producto.Where(a => a.IdCategoriaProducto == idCategoria).ToListAsync();
        public async Task<List<CategoriaProducto>> GetCategory() => await _context.CategoriaProducto.ToListAsync();
        public async Task<CategoriaProducto> GetCategoryByName(string nombreCategoria) => await _context.CategoriaProducto.Where(x => x.nombre == nombreCategoria).FirstOrDefaultAsync();
        public async Task AddCategory(CategoriaProducto categoria) 
        {
            await _context.CategoriaProducto.AddAsync(categoria);
            await _context.SaveChangesAsync();
        }
        public async Task AddProduct(Producto producto) 
        {
            await _context.Producto.AddAsync(producto);
            await _context.SaveChangesAsync();
        } 
        public async Task UpdateProduct(Producto producto)
        {
            _context.Producto.Update(producto);
            await _context.SaveChangesAsync();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using PruebaTecnica.Dtos.Models;

namespace PruebaTecnica.Infrastructure.Context
{
    public class AlmacenContext : DbContext
    {
        public AlmacenContext(DbContextOptions<AlmacenContext> options) : base(options)
        {
        }

        public DbSet<CategoriaProducto> CategoriaProducto { get; set; }
        public DbSet<Producto> Producto { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
    }
}
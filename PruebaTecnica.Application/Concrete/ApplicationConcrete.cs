﻿using PruebaTecnica.Dtos.Interfaces;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Request;
using PruebaTecnica.Dtos.Response;

namespace PruebaTecnica.Application.Concrete
{
    public class ApplicationConcrete : IAplication
    {
        private readonly IAlmacen _almacenInfrastructure;

        public ApplicationConcrete(IAlmacen almacen)
        {
            _almacenInfrastructure = almacen;
        }

        public async Task<List<Producto>> GetProductos()
        {
            var productos = await _almacenInfrastructure.GetProductos();
            var categorias = await _almacenInfrastructure.GetCategory();
            foreach (var item in productos)
            {
                item.CategoriaProducto = categorias.Where(x => x.IdCategoriaProducto == item.IdCategoriaProducto).First();
            }
            return productos;
        }
        public async Task<List<CategoriaProducto>> GetCategorias() => await _almacenInfrastructure.GetCategory();
        public async Task<List<Producto>> GetProductosByCategory(int idCategoria) => await _almacenInfrastructure.GetProductosByCategory(idCategoria);
        public async Task<ResponseModel<Producto>> AddProduct(ProductRequest producto)
        {
            var categoria = await _almacenInfrastructure.GetCategoryByName(producto.nombreCategoria);
            Producto newProduct;
            if(categoria != null)
                newProduct = new Producto(producto.nombre, categoria.IdCategoriaProducto, producto.estado);
            else
            {
                categoria = new CategoriaProducto(producto.nombreCategoria);
                await _almacenInfrastructure.AddCategory(categoria);
                newProduct = new Producto(producto.nombre, categoria.IdCategoriaProducto, producto.estado);
            }
            await _almacenInfrastructure.AddProduct(newProduct);
            return new ResponseModel<Producto>(true, "Exitoso", newProduct);
        }
        public async Task<ResponseModel<Producto>> UpdateProduct(UpdateProductRequest productoUpdate)
        {
            var producto = await _almacenInfrastructure.GetProductById(productoUpdate.IdProducto);
            if (producto == null)
                return new ResponseModel<Producto>(false, "Producto no encontrado", null);

            producto.nombre = productoUpdate.nombre;
            producto.estado = productoUpdate.estado;
            producto.IdCategoriaProducto = productoUpdate.IdCategoriaProducto;
            await _almacenInfrastructure.UpdateProduct(producto);
            return new ResponseModel<Producto>(true, "Exitoso", producto);
        }

    }
}

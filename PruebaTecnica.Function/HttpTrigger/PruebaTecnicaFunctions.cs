using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using PruebaTecnica.Dtos.Request;
using System.Threading.Tasks;

namespace PruebaTecnica.Function.HttpTrigger
{
    public class PruebaTecnicaFunctions
    {
        private readonly IMediator _mediator;

        public PruebaTecnicaFunctions(IMediator mediator)
        {
            _mediator = mediator;
        }

        [FunctionName("GetProductos")]
        public async Task<IActionResult> GetProductos(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)] GetProductosRequest request, HttpRequest req,
            ILogger log)
        {
            try
            {
                return new OkObjectResult(await _mediator.Send(request));
            }
            catch (System.Exception ex)
            {

            }
            return new BadRequestResult();
        }
        [FunctionName("GetCategorias")]
        public async Task<IActionResult> GetCategorias(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)] GetCategoriasRequest request, HttpRequest req,
            ILogger log) => new OkObjectResult(await _mediator.Send(request));
        [FunctionName("AddProduct")]
        public async Task<IActionResult> AddProduct(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)] ProductRequest request, HttpRequest req,
            ILogger log) => new OkObjectResult(await _mediator.Send(request));
        [FunctionName("UpdateProduct")]
        public async Task<IActionResult> UpdateProduct(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)] UpdateProductRequest request, HttpRequest req,
            ILogger log) => new OkObjectResult(await _mediator.Send(request));

    }
}

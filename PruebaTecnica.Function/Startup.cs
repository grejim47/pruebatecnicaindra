﻿using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using PruebaTecnica.Services;
[assembly: FunctionsStartup(typeof(PruebaTecnica.Function.Startup))]
namespace PruebaTecnica.Function
{
    public class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            builder.Services.AddServices();
        }
    }
}

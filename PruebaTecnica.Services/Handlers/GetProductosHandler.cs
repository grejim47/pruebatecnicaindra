﻿using MediatR;
using PruebaTecnica.Dtos.Interfaces;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnica.Services.Handlers
{
    public class GetProductosHandler : IRequestHandler<GetProductosRequest, List<Producto>>
    {
        private readonly IAplication _app;

        public GetProductosHandler(IAplication app)
        {
            _app = app;
        }

        public async Task<List<Producto>> Handle(GetProductosRequest request, CancellationToken cancellationToken) => await _app.GetProductos();
    }
}

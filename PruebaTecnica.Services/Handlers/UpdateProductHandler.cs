﻿using MediatR;
using PruebaTecnica.Dtos.Interfaces;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Request;
using PruebaTecnica.Dtos.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnica.Services.Handlers
{
    public class UpdateProductHandler : IRequestHandler<UpdateProductRequest, ResponseModel<Producto>>
    {
        private readonly IAplication _app;

        public UpdateProductHandler(IAplication app)
        {
            _app = app;
        }

        public async Task<ResponseModel<Producto>> Handle(UpdateProductRequest request, CancellationToken cancellationToken) => await _app.UpdateProduct(request);
    }
}

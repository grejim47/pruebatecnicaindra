﻿using MediatR;
using PruebaTecnica.Dtos.Interfaces;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnica.Services.Handlers
{
    public class GetCategoriasHandler : IRequestHandler<GetCategoriasRequest, List<CategoriaProducto>>
    {
        private readonly IAplication _app;

        public GetCategoriasHandler(IAplication app)
        {
            _app = app;
        }
        public async Task<List<CategoriaProducto>> Handle(GetCategoriasRequest request, CancellationToken cancellationToken) => await _app.GetCategorias();
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Request;
using PruebaTecnicaFront.Models;
using RestSharp;
using System.Diagnostics;

namespace PruebaTecnicaFront.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            RestClient restClient = new RestClient("http://localhost:7072/api/GetProductos");
            RestRequest request = new RestRequest();
            request.Method = Method.Get;
            var response = await restClient.ExecuteAsync(request);
            var model = JsonConvert.DeserializeObject<List<Producto>>(response.Content);
            return View(model);
        }

        public async Task<IActionResult> AgregarProducto()
        {
            RestClient restClient = new RestClient("http://localhost:7072/api/GetCategorias");
            RestRequest request = new RestRequest();
            request.Method = Method.Get;
            var response = await restClient.ExecuteAsync(request);
            var model = JsonConvert.DeserializeObject<List<CategoriaProducto>>(response.Content);
            return View(model);
        }
        public async Task<IActionResult> EditProduct(int id)
        {
            ViewBag.idProducto = id;
            RestClient restClient = new RestClient("http://localhost:7072/api/GetCategorias");
            RestRequest request = new RestRequest();
            request.Method = Method.Get;
            var response = await restClient.ExecuteAsync(request);
            var model = JsonConvert.DeserializeObject<List<CategoriaProducto>>(response.Content);
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> AgregarProducto(string nombreProducto, string selCat, string nuevaCat)
        {
            ProductRequest requestProduct = new ProductRequest();
            if (selCat != "-1")
                nuevaCat = selCat;

            requestProduct.nombreCategoria = nuevaCat;
            requestProduct.nombre = nombreProducto;
            requestProduct.estado = "A";

            RestClient restClient = new RestClient("http://localhost:7072/api/AddProduct");
            RestRequest request = new RestRequest();
            request.Method = Method.Post;
            request.AddJsonBody(JsonConvert.SerializeObject(requestProduct));
            var response = await restClient.ExecuteAsync(request);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public async Task<IActionResult> EditProduct(string nombreProducto, int selCat, int idProducto)
        {
            RestClient restClient = new RestClient("http://localhost:7072/api/GetProductos");
            RestRequest request = new RestRequest();
            request.Method = Method.Get;
            var response = await restClient.ExecuteAsync(request);
            var model = JsonConvert.DeserializeObject<List<Producto>>(response.Content);

            var producto = model.Where(x => x.IdProducto == idProducto).FirstOrDefault();
            producto.nombre = nombreProducto;
            producto.IdCategoriaProducto = selCat;
            restClient = new RestClient("http://localhost:7072/api/UpdateProduct");
            request = new RestRequest();
            request.Method = Method.Post;
            request.AddJsonBody(JsonConvert.SerializeObject(producto));
            response = await restClient.ExecuteAsync(request);
            return RedirectToAction("Index");
        }
        
        public async Task<IActionResult> DeleteProduct(int id)
        {
            RestClient restClient = new RestClient("http://localhost:7072/api/GetProductos");
            RestRequest request = new RestRequest();
            request.Method = Method.Get;
            var response = await restClient.ExecuteAsync(request);
            var model = JsonConvert.DeserializeObject<List<Producto>>(response.Content);

            var producto = model.Where(x => x.IdProducto == id).FirstOrDefault();
            producto.estado = producto.estado == "A" ? "I" : "A";

            restClient = new RestClient("http://localhost:7072/api/UpdateProduct");
            request = new RestRequest();
            request.Method = Method.Post;
            request.AddJsonBody(JsonConvert.SerializeObject(producto));
            response = await restClient.ExecuteAsync(request);
            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
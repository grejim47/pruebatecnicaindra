﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnica.Dtos.Models
{
    public class Producto
    {
        [Key]
        public int IdProducto { get; set; }
        public string nombre { get; set; }
        public int IdCategoriaProducto { get; set; }
        public string estado { get; set; }
        [NotMapped]
        public CategoriaProducto CategoriaProducto { get; set; }
        public Producto()
        {

        }
        public Producto(string nombre, int idCategoriaProducto, string estado)
        {
            this.nombre = nombre;
            this.estado = estado;
            this.IdCategoriaProducto = idCategoriaProducto;
        }
    }
}

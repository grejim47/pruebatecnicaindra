﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnica.Dtos.Models
{
    public class CategoriaProducto
    {
        [Key]
        public int IdCategoriaProducto { get; set; }
        public string nombre { get; set; }
        public CategoriaProducto()
        {

        }
        public CategoriaProducto(string nombre)
        {
            this.nombre = nombre;
        }
    }
}

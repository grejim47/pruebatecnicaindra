﻿using MediatR;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnica.Dtos.Request
{
    public class UpdateProductRequest : IRequest<ResponseModel<Producto>>
    {
        public int IdProducto { get; set; }
        public string nombre { get; set; }
        public int IdCategoriaProducto { get; set; }
        public string estado { get; set; }
    }
}

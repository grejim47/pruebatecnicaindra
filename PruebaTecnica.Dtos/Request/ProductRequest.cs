﻿using MediatR;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnica.Dtos.Request
{
    public class ProductRequest : IRequest<ResponseModel<Producto>>
    {
        public string nombre { get; set; }
        public string nombreCategoria { get; set; }
        public string estado { get; set; }
    }
}

﻿using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Request;
using PruebaTecnica.Dtos.Response;

namespace PruebaTecnica.Dtos.Interfaces
{
    public interface IAplication
    {
        Task<List<Producto>> GetProductos();
        Task<List<CategoriaProducto>> GetCategorias();
        Task<List<Producto>> GetProductosByCategory(int idCategoria);
        Task<ResponseModel<Producto>> AddProduct(ProductRequest producto);
        Task<ResponseModel<Producto>> UpdateProduct(UpdateProductRequest productoUpdate);
    }
}

﻿using PruebaTecnica.Dtos.Models;

namespace PruebaTecnica.Dtos.Interfaces
{
    public interface IAlmacen
    {
        Task<List<Producto>> GetProductos();
        Task<List<Producto>> GetProductosByCategory(int idCategoria);
        Task<List<CategoriaProducto>> GetCategory();
        Task<CategoriaProducto> GetCategoryByName(string nombreCategoria);
        Task AddCategory(CategoriaProducto categoria);
        Task AddProduct(Producto producto);
        Task<Producto> GetProductById(int idproducto);
        Task UpdateProduct(Producto producto);
    }
}
